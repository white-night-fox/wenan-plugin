# 云崽文案插件

#### 介绍
云崽回复类文案插件

### 使用Git安装（推荐）

请使用 git 进行安装，以方便后续升级。  
1. 在 Yunzai-Bot 根目录打开终端，运行指令：

```
git clone --depth=1 https://gitee.com/white-night-fox/wenan-plugin.git ./plugins/wenan-plugin/
```
2. 重启 Yunzai-Bot 后即可使用。
---
发送 `#文案帮助` 获取功能列表。

### 特别感谢

[酸菜咸鱼](https://gitee.com/suancaixianyu)
