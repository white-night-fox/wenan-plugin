import plugin from '../../../lib/plugins/plugin.js';  
import fetch from 'node-fetch';  
  
export class InsultBook extends plugin { // 类名可以更改，但这里保留了以便示例  
  constructor() {  
    super({  
      name: '网易云热评', // 名称可以根据您的需要更改  
      dsc: '获取网易云热评',  
      event: 'message',  
      priority: 5000,  
      rule: [{  
        reg: /^#网易云热评/, // 正则表达式匹配指令  
        fnc: 'wangyiyunreping'   // 调用的函数名  
      }]  
    });  
  }  
  
  async wangyiyunreping(e) {  
    try {  
      // 构造API请求URL  
      let url = 'https://v.api.aa1.cn/api/api-wenan-wangyiyunreping/index.php?aa1=json';  
        
      // 发送HTTP请求并等待响应  
      let res = await fetch(url);  
        
      // 检查响应状态是否正常  
      if (!res.ok) {  
        throw new Error(`HTTP error! Status: ${res.status}`);  
      }  
        
      // 解析JSON响应  
      let data = await res.json();  
        
      // 检查响应数组是否非空，并从中提取第一条热评  
      if (Array.isArray(data) && data.length > 0 && typeof data[0] === 'object' && 'wangyiyunreping' in data[0]) {  
        // 假设'wangyiyunreping'字段包含要发送的消息  
        await this.reply(data[0].wangyiyunreping); // 只回复数组中的第一条热评  
      } else {  
        // 如果没有找到'wangyiyunreping'字段、响应不是数组或数组为空，则发送错误消息  
        throw new Error('API响应中没有找到预期的热评内容');  
      }  
    } catch (err) {  
      // 如果在请求或处理响应时发生错误，则记录错误并发送错误消息给用户  
      console.error('[网易云热评] 请求API时发生错误:', err);  
      await this.reply('请求网易云热评时出错，请稍后再试');  
    }  
  }
}