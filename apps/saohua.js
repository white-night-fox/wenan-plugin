import Plugin from '../../../lib/plugins/plugin.js'; // 确保Plugin类名的大小写与实际匹配  
  
export class ComfortMessage extends Plugin {  
  constructor() {  
    super({  
      name: '骚话文案',  
      dsc: '提供骚话的文案',  
      event: 'message',  
      priority: 5000,  
      rule: [  
        {  
          reg: /^#(骚话文案|saohua|骚话)\b/,// 修改正则表达式以匹配“骚话文案”请求  
          fnc: 'saohua' // 修改方法名为getComfortMessage  
        },  
      ]  
    });  
  }  
  
  async saohua(e) {  
    // 不再需要处理短网址相关的逻辑  
    try {  
      const res = await fetch('https://v.api.aa1.cn/api/api-saohua/index.php?type=json');  
      if (!res.ok) {  
        throw new Error(`HTTP error! Status: ${res.status}`);  
      }  
      const jsonResponse = await res.json();  
      if (jsonResponse && jsonResponse.saohua) {  
        // 假设saohua字段包含骚话文案  
        await this.reply(jsonResponse.saohua); // 直接回复骚话文案  
      } else {  
        // 处理saohua字段不存在或为空的情况  
        await this.reply('未找到骚话文案');  
      }  
    } catch (err) {  
      // 请求失败或JSON解析失败时的处理  
      console.error('[骚话文案] 接口请求失败或解析错误:', err); // 使用console.error记录错误  
      await this.reply('骚话文案接口请求失败或解析错误,请联系管理员检查接口');  
    }  
  }  
}
