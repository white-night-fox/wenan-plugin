import Plugin from '../../../lib/plugins/plugin.js'; // 确保Plugin类名的大小写与实际匹配  
  
export class ComfortMessage extends Plugin {  
  constructor() {  
    super({  
      name: '朋友圈文案',  
      dsc: '提供朋友圈每日一言',  
      event: 'message',  
      priority: 5000,  
      rule: [  
        {  
          reg: `^#(.*)(朋友圈文案|朋友圈|朋友圈一言|pyq)(.*)`, // 修改正则表达式以匹配“朋友圈文案”请求  
          fnc: 'getpyq' // 修改方法名为pyq 
        },  
      ]  
    });  
  }  
  
  async getpyq(e) {  
    // 不再需要处理短网址相关的逻辑  
    try {  
      const res = await fetch('https://v.api.aa1.cn/api/pyq/index.php?aa1=json');  
      if (!res.ok) {  
        throw new Error(`HTTP error! Status: ${res.status}`);  
      }  
      const jsonResponse = await res.json();  
      if (jsonResponse && jsonResponse.pyq) {  
        // 假设pyq字段包含朋友圈文案  
        await this.reply(jsonResponse.pyq); // 直接回复朋友圈文案  
      } else {  
        // 处理pyq字段不存在或为空的情况  
        await this.reply('未找到朋友圈文案');  
      }  
    } catch (err) {  
      // 请求失败或JSON解析失败时的处理  
      console.error('[朋友圈一言] 接口请求失败或解析错误:', err); // 使用console.error记录错误  
      await this.reply('朋友圈一言接口请求失败或解析错误,请联系管理员检查接口');  
    }  
  }  
}
