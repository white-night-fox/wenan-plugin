import lodash from 'lodash'
import plugin from '../../../lib/plugins/plugin.js'
import cards from '../data/jt.js'

export class djt extends plugin {
  constructor () {
    super({
      name: 'jt',
      dsc: '鸡汤',
      event: 'message',
      priority: 5000,
      rule: [
        {
          reg: '^#?(鸡汤)(\\s|$)',
          fnc: 'jt'
        }
      ]
    })
    this.prefix = 'L:other:jt:'
  }

  async jt () {
    const card = lodash.sample(cards)

    let msg = `${card?.name}\n${card?.dsc}`
    if (this.e.isGroup) {
      msg = '\n' + msg
    }
    await this.reply(msg, false, { at: true })
  }
}