import Plugin from '../../../lib/plugins/plugin.js'; // 确保Plugin类名的大小写与实际匹配  
  
export class ComfortMessage extends Plugin {  
  constructor() {  
    super({  
      name: '安慰文案',  
      dsc: '提供安慰人心的文案',  
      event: 'message',  
      priority: 5000,  
      rule: [  
        {  
          reg: `^#(.*)(安慰文案)(.*)`, // 修改正则表达式以匹配“安慰文案”请求  
          fnc: 'getComfortMessage' // 修改方法名为getComfortMessage  
        },  
      ]  
    });  
  }  
  
  async getComfortMessage(e) {  
    // 不再需要处理短网址相关的逻辑  
    try {  
      const res = await fetch('https://v.api.aa1.cn/api/api-wenan-anwei/index.php?type=json');  
      if (!res.ok) {  
        throw new Error(`HTTP error! Status: ${res.status}`);  
      }  
      const jsonResponse = await res.json();  
      if (jsonResponse && jsonResponse.anwei) {  
        // 假设anwei字段包含安慰文案  
        await this.reply(jsonResponse.anwei); // 直接回复安慰文案  
      } else {  
        // 处理anwei字段不存在或为空的情况  
        await this.reply('未找到安慰文案');  
      }  
    } catch (err) {  
      // 请求失败或JSON解析失败时的处理  
      console.error('[安慰文案] 接口请求失败或解析错误:', err); // 使用console.error记录错误  
      await this.reply('安慰文案接口请求失败或解析错误,请联系管理员检查接口');  
    }  
  }  
}
