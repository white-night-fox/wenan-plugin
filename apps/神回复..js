import Plugin from '../../../lib/plugins/plugin.js'; // 确保Plugin类名的大小写与实际匹配  
  
export class ComfortMessage extends Plugin {  
  constructor() {  
    super({  
      name: '神回复文案',  
      dsc: '提供神回复每日一言',  
      event: 'message',  
      priority: 5000,  
      rule: [  
        {  
          reg: `^#(.*)(神回复文案|神回复)(.*)`, // 修改正则表达式以匹配“神回复文案”请求  
          fnc: 'getshenhuifu' // 修改方法名为shenhuifu 
        },  
      ]  
    });  
  }  
  
  async getshenhuifu(e) {  
    try {  
      const res = await fetch('https://v.api.aa1.cn/api/api-wenan-shenhuifu/index.php?aa1=json');  
      if (!res.ok) {  
        throw new Error(`HTTP error! Status: ${res.status}`);  
      }  
      const jsonResponse = await res.json();  
      if (Array.isArray(jsonResponse) && jsonResponse.length > 0 && jsonResponse[0].shenhuifu) {  
        // 替换<br>为换行符  
        const shenhuifuWithNewlines = jsonResponse[0].shenhuifu.replace(/<br>/g, '\n');  
        // 回复神回复文案，其中<br>已被替换为换行符  
        await this.reply(shenhuifuWithNewlines);  
      } else {  
        await this.reply('未找到神回复文案');  
      }  
    } catch (err) {  
      console.error('[神回复] 接口请求失败或解析错误:', err);  
      await this.reply('神回复接口请求失败或解析错误, 请联系管理员检查接口');  
    }  
  }
}