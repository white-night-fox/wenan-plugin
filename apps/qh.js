import plugin from '../../../lib/plugins/plugin.js';  
import fetch from 'node-fetch';  
  
export class wenan extends plugin {  
    constructor() {  
        super({  
            name: '文案情话',  
            dsc: '情话',  
            event: 'message',  
            priority: 5000,  
            rule: [  
                {  
                    reg: /#情话/,  
                    fnc: 'qh'  // 确保这里的fnc对应正确的方法名  
                }  
            ]  
        });  
    }  
  
    async qh(e) {  
        logger.info('[用户命令]', e.msg);  
        const url = 'https://api.vvhan.com/api/love?type=json';  
        try {  
            const response = await fetch(url);  
            if (!response.ok) {  
                throw new Error('网络响应不是ok');  
            }  
            const data = await response.json();  
            if (!data.success) {  
                throw new Error('API请求失败');  
            }  
            const ishan = data.ishan;  
            logger.info(`[接口结果] 情话：${ishan}`);  
            await this.reply(`${ishan}`);  
        } catch (error) {  
            logger.error('[情话] 发生错误:', error);  
            await this.reply('获取情话时发生错误');  
        }  
    }  
}