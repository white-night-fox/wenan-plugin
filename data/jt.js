export default[
    {
        name:'心灵鸡汤1',
        dsc:'在这世上，最不缺的就是玻璃心。你需要让自己的内心更强大，你需要让自己的能力配得上自己的梦想。希望你有傲人的资本，而不是自甘堕落。'
    },
    {
        name:'心灵鸡汤2',
        dsc:'放下你的浮躁，放下你的懒惰，放下你的三分钟热度，放空你禁不住诱惑的大脑，放开你容易被任何事物吸引的眼睛，放淡你什么都想聊两句八卦的嘴巴，静下心来好好做你该做的事，该好好努力了。'
    },
    {
        name:'心灵鸡汤3',
        dsc:'再长的路，一步步也能走完；再短的路，不迈开双脚也无法到达。还在咬牙坚持的你，请不要泄气。你的日积月累，早晚会成为别人的望尘莫及。'
    },
    {
        name:'心灵鸡汤4',
        dsc:'与其担心未来，不如现在好好努力。这条路上，只有奋斗才能给你安全感。不要轻易把梦想寄托在某个人身上，也不要太在乎身旁的耳语，因为未来是你自己的，只有你自己才能给你自己最大的安全感。'
    },
    {
        name:'心灵鸡汤5',
        dsc:'至于未来会怎样，要用力走下去才知道，记住，不要轻易放弃，自己的命运在自己手中，跌倒失败有何惧，只要干不死，就往死里干，反正路还长，天总会亮。。'
    },
    {
        name:'心灵鸡汤6',
        dsc:'一个人的豁达，体现在落魄的时候。一个人的涵养，体现在愤怒的时候。一个人的体贴，体现在悲伤的时候。一个人的成熟，体现在抉择的时候。谁都愿意做自己喜欢的事情，可一个人做该做的事情，才叫成长。。'
    },
    {
        name:'心灵鸡汤7',
        dsc:'如果你不想苦一辈子，就要先苦一阵子。你现在有什么样的付出，将来你的人生就会呈现什么样的风景。每一个你所期待的美好未来，都必须依靠一个努力踏实的现在。'
    },
    {
        name:'心灵鸡汤8',
        dsc:'总有一天，你会明白，你的委屈要自己消化，你的故事不用逢人就讲起，真正理解你的人没有几个，大多数人只会站在他们自己的立场，偷看你的笑话，你能做的就是，把秘密藏起来，然后一步一步变得越来越强大。'
    },
    {
        name:'心灵鸡汤9',
        dsc:'碰到一点压力就把自己变成不堪重负的样子，碰到一点不确定性就把前途描摹成黯淡无光的样子，碰到一点不开心就把它搞得似乎是自己这辈子最黑暗的时候，大概都只是为了自己不去走而干脆放弃明天找的最拙劣的借口！'
    },
    {
        name:'心灵鸡汤10',
        dsc:'有人帮你是幸运，学会心怀欢喜与感恩。无人帮你是命运，学会坦然面对与承担。人生的必修课是接受无常，人生的选修课是放下执著。当生命陷落的时候请记得，你必须跌到你从未经历过的谷底，才能站上你从未到达过的高峰。'
    },
    {
        name:'心灵鸡汤11',
        dsc:'无论你正经历着什么，过得是否开心，世界不会因为你的疲惫，而停下它的脚步。那些你不能释怀的人与事，总有一天会在你念念不忘之时早已遗忘。无论黑夜多么漫长不堪，黎明始终会如期而至。'
    },
    {
        name:'心灵鸡汤12',
        dsc:'你要相信，你生命里遇到的每个人每件事，都有它的价值和好处，有些人教会你爱，有些事教会你成长，哪怕只是浅浅在你的路途中留下印记，也是一笔难能可贵的财富。至少在以前某个时刻，你明白了生活，你懂得了自己。'
    },
    {
        name:'心灵鸡汤13',
        dsc:'人生几十年，酸甜苦辣咸，各种滋味都有，打开心灵的窗，去呼吸一下外面的新鲜空气。别让自己活得太累。应该学着想开、看淡，学着不强求，学着深藏。适时放松自己，寻找宣泄，给疲惫的心灵解解压。'
    },
    {
        name:'心灵鸡汤14',
        dsc:'人生，要懂得感恩。感恩，不只是感谢大恩大德，而是一种生活态度，一种善良的人性美。感恩一切顺境，给我们带来了幸福；感恩一切逆境，增强了我们追求幸福的能力。心存感恩，心灵才会获得宁静和安详；生活中才会少了许多怨气和烦恼。一颗懂得感恩的心，其实就是莫大的幸福和人生的智慧。'
    },
    {
        name:'心灵鸡汤15',
        dsc:'珍惜每一天的时光，努力过充实的生活。你会找到属于你的价值和快乐。比如：多读书，多交流，和旅行有一次约会。人生，没有永远的伤痛，再深的痛，伤口总会痊愈。人生，不会永远都只有痛，学会去享受生活中的甜乐。'
    },
    {
        name:'心灵鸡汤16',
        dsc:'愿你从此不对新事畏惧，不会重蹈覆辙，再也不感情沉沦，不为熬夜后会，相信有人会陪你颠沛流离，如果没有，愿你成为自己的太阳。'
    },
    {
        name:'心灵鸡汤17',
        dsc:'很多时候，幸福是朴素的，微小的，甚至不易觉察。学会感受幸福，才能拥有更多的幸福。'
    },
    {
        name:'心灵鸡汤18',
        dsc:'一个人，注定只能经历一种人生。而我一直想把一生当做几辈子来过，所以会常常觉得来不及。也所以，会到了某个时候，总想把一段人生掐断，重新开始。而其实，人生一旦开始，就没有回头路。'
    },
    {
        name:'心灵鸡汤19',
        dsc:'你的人生永远不会辜负你的，那些转错的弯，那些走错的路，那些流下的泪水，那些留下的伤痕，全都会让你成为独一无二的自己。'
    },
    {
        name:'心灵鸡汤20',
        dsc:'一辈子不长，从今天开始，每天微笑吧，世上除了生死，都是小事。不管遇到了什么烦心事，都不要自己为难自己，每晚睡前，原谅所有的人和事。闭上眼睛，清理你的心，过去的就让它过去吧。无论今天发生多么糟糕的事，都不应该感到悲伤。今天永远只是起跑线。记住一句话：越努力，越幸运。'
    },
    {
        name:'心灵鸡汤21',
        dsc:'真正成功的人生，不在于成就的大小，不在于地位的高低，不在于金钱的多少，不在于名气的有无。而在于：是否努力地完成自我，喊出属于自己的声音，活出属于自己的活法，走出属于自己的道路。'
    },
    {
        name:'心灵鸡汤22',
        dsc:'生活最好的治愈系是工作，一旦忙碌起来，什么坏情绪都会被抛之脑后，疲惫到没精力矫情。'
    },
    {
        name:'心灵鸡汤23',
        dsc:'生活从来都是公平的，你未来的模样，藏在你现在的努力里。有规划的人生，真的会比浑浑噩噩精彩一万倍。新的每一天，请勇敢去尝试，拼命去追寻。'
    },
    {
        name:'心灵鸡汤24',
        dsc:'树大容易招风，也容易生虫。要想做一棵大树，就要忍受一些风吹虫咬。虽然会痛，但不能因为疼痛就不生长。人亦如此，越是出色的人，越会有小人绊脚，出恶语，或不怀好意，但即便如此，我们仍要成长，仍要上进。对于不喜欢我们的人，要做的只有：站得更挺拔，活得更好。'
    },
    {
        name:'心灵鸡汤25',
        dsc:'要努力啊，不然怎么嘲笑那些嘲笑过你的人。'
    },
    {
        name:'心灵鸡汤26',
        dsc:'往事不回头，余生不将就。奉劝自己不再纠结，活在当下。'
    },
    {
        name:'心灵鸡汤27',
        dsc:'世界让我们遍体鳞伤，但伤口长出的却是翅膀，以后要懂一直飞翔才是你的宿命。'
    },
    {
        name:'心灵鸡汤28',
        dsc:'别再问我工作何必这么拼，我之所以这么努力，因为我想要的生活比你的贵…'
    },
    {
        name:'心灵鸡汤29',
        dsc:'时光永远不会倒流更不会为谁停留，踏踏实实的走好每一步，即使错了，大不了从头来过，但是一定不要在最好的年华里，辜负最好的自己！想要得到任何东西，都要有同等的付出，没有无缘无故的成功，更不会有从天而降的馅饼，人生短暂，且行且珍惜！'
    },
    {
        name:'心灵鸡汤30',
        dsc:'每个人都会累，没人能为你承担所有伤悲，人总有一段时间要学会自己长大。'
    },
    {
        name:'心灵鸡汤31',
        dsc:'顺从自己的内心而活，才是最好的生活。人生这条路，走着走着就不知所向，你以为的和你的身边都以为的目的地，却陡然急转直下，甚至与构想的完全不着边际，可是这就是人生，谁也不能确定的明天的明天。所以过了今天就好了。'
    },
    {
        name:'心灵鸡汤32',
        dsc:'生活最大的幸福就是，坚信有人爱着我。对于过去，不可忘记，但要放下。因为有明天，今天永远只是起跑线。生活简单就迷人，人心简单就幸福；学会简单其实就不简单。'
    },
    {
        name:'心灵鸡汤33',
        dsc:'青年，最大的资本不是经验丰富、胸有成竹，而是敢于做梦、勇于试错。在生命力最旺盛的日子里，就该像爬山虎一样，向着心中的梦想不断向上攀沿，把青春的绿色铺满征途。'
    },
]